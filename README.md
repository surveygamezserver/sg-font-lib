All font faces supported in SurveyGamez.

While web uses woff2, Android 4.x still needs ttf.

https://github.com/google/woff2

This tool has been used for doing woff2 to ttf conversion.
